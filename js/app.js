var Contacts = Em.Application.create();

Contacts.Contact = Em.Object.extend(Em.Copyable, {
	firstName: '',
	firstName: '',
	phoneNumbers: [],

	fullName: function() {
		if (this.get('firstName') === '') {
			return this.get('lastName');
		} else if (this.get('lastName') === '') {
			return this.get('firstName');
		} else {
			return this.get('lastName') + ', ' + this.get('firstName');
		}
	}.property('firstName', 'lastName'),

	hasName: function() {
		return this.get('firstName') !== '' || this.get('firstName') !== '';
	}.property('firstName', 'lastName'),

	copy: function() {
		return Contacts.Contact.create(this);
	},

	merge: function(contact) {
		this.setProperties(contact);
	}
});

Contacts.contactsController = Em.ArrayController.create({
	content: [],
	selection: null,

	loadContacts: function(options) {
		var self = this;
		$.ajax($.extend({
			success: function(data) {
				var contacts = data.contacts.map(function(contact) {
					return Contacts.Contact.create(contact);
				});
				self.set('content', contacts);
			}
		}, options));
	},

	createContact: function() {
		this.set('selection', this.pushObject(Contacts.Contact.create()));
	}
});

Contacts.contactController = Em.Object.create({
	content: null,
	parent: null,

	hasContent: function() {
		return this.get('content') !== null;
	}.property('content'),

	hasNoContent: function() {
		return this.get('content') === null;
	}.property('content'),

	selectContact: function() {
		var selection = Contacts.contactsController.get('selection');

		this.set('parent', selection);
		this.set('content', selection.copy());
	},

	saveContact: function() {
		var copy = this.get('content');
		var selection = this.get('parent');
		selection.merge(copy);
	},

	deleteContact: function() {
		var selection = this.get('parent');
		Contacts.contactsController.popObject(selection);
		if (selection === Contacts.contactsController.get('selection')) {
			Contacts.contactsController.set('selection', Contacts.contactsController.get('content')[0]);
		}
	},

	_selectionDidChange: function() {
		this.set('content', null);
		this.set('parent', null);
	}.observes('Contacts.contactsController.selection')
});

Contacts.ContactList = Em.CollectionView.extend({
	itemViewClass: Em.View.extend({
		isSelected: function() {
			var person = this.get('content');
			var selection = Contacts.contactsController.get('selection');

			return person === selection;
		}.property('Contacts.contactsController.selection'),

		click: function(event) {
			Contacts.contactsController.set('selection', this.get('content'));
			Contacts.contactController.set('content', null);
		}
	})
});

Contacts.contactsController.loadContacts({
	url: 'contacts.json',
	type: 'json'
})